//
//  AppDelegate.h
//  DynamicPlaceholderTextfieldExample
//
//  Created by yonathan justianus muliawan on 5/16/16.
//  Copyright © 2016 liang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

