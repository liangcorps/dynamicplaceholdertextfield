//
//  YV_DynamicPlaceholderTF.h
//  ObjectiveCTestLab
//
//  Created by yonathan justianus muliawan on 5/16/16.
//  Copyright © 2016 liang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YV_DPTF.h"
@interface YV_DynamicPlaceholderTF : UIView <dynamicPlaceholderDelegate>

@property (nonatomic, weak) id <UITextFieldDelegate> delegate;
@property (nonatomic, strong)YV_DPTF *textField;

@property (nonatomic, strong)UIFont *selectedPlaceholderFont;
@property (nonatomic, strong)UIFont *unSelectedPlaceholderFont;
@property (nonatomic, strong)UIColor *selectedPlaceholderColor;
@property (nonatomic, strong)UIColor *unSelectedPlaceholderColor;

- (void)setPlaceholder:(NSString *)placeholder;

- (void)becomeFirstResponder;
- (void)resignFirstResponder;
@end
