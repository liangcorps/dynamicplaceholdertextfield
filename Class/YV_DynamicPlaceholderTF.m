//
//  YV_DynamicPlaceholderTF.m
//  ObjectiveCTestLab
//
//  Created by yonathan justianus muliawan on 5/16/16.
//  Copyright © 2016 liang. All rights reserved.
//

#import "YV_DynamicPlaceholderTF.h"

@interface YV_DynamicPlaceholderTF()

@property (nonatomic, strong)UILabel *lblPlaceholder;

@end
@implementation YV_DynamicPlaceholderTF{
    CGSize size;
    BOOL textfieldIsActive;
}

@synthesize selectedPlaceholderFont = _selectedPlaceholderFont;
@synthesize unSelectedPlaceholderFont = _unSelectedPlaceholderFont;
@synthesize selectedPlaceholderColor = _selectedPlaceholderColor;
@synthesize unSelectedPlaceholderColor = _unSelectedPlaceholderColor;
#pragma mark - life cycle & override
- (instancetype)init{
    self = [super init];
    if (self) {
        [self setProperties];
        [self loadUI];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setProperties];
        [self loadUI];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setProperties];
        [self loadUI];
    }
    return self;
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    if (!CGSizeEqualToSize(frame.size, size)) {
        size = frame.size;
        [self relayout];
    }
}

#pragma mark - starter method
- (void)setProperties{
    textfieldIsActive = NO;
    self.backgroundColor = [UIColor clearColor];
}
- (void)loadUI{
    [self addSubview:self.lblPlaceholder];
    [self addSubview:self.textField];
}

#pragma mark - methods
- (void)relayout{
    CGRect bound = self.bounds;
    if (self.textField.text.length > 0 || self.textField.isFirstResponder) {
        bound.size.height = 12;
    }
    self.lblPlaceholder.frame = bound;
    bound = self.bounds;
    bound.origin.y = 12;
    bound.size.height -= 12;
    self.textField.frame = bound;
}

- (void)setDelegate:(id<UITextFieldDelegate>)delegate{
    _delegate = delegate;
    self.textField.delegate = delegate;
}

- (void)setPlaceholder:(NSString *)placeholder{
    self.lblPlaceholder.text = placeholder;
}

- (void)becomeFirstResponder{
    [self.textField becomeFirstResponder];
}
- (void)DPTFBecomeFirstResponder{
    textfieldIsActive = YES;
    CGRect bound = self.bounds;
    bound.size.height = 12;
    [UIView animateWithDuration:0.25 animations:^{
        [self refreshPlaceholderState];
        self.lblPlaceholder.frame = bound;
    }];
}
- (void)resignFirstResponder{
    [self.textField resignFirstResponder];
}
- (void)DPTFResignFirstResponder{
    textfieldIsActive = NO;
    if (self.textField.text.length > 0) {
        return;
    }
    [UIView animateWithDuration:0.25 animations:^{
        [self refreshPlaceholderState];
        self.lblPlaceholder.frame = self.bounds;
    }];
}

- (void)refreshPlaceholderState{
    self.lblPlaceholder.font = (textfieldIsActive)?self.selectedPlaceholderFont:self.unSelectedPlaceholderFont;
    self.lblPlaceholder.textColor = (textfieldIsActive)?self.selectedPlaceholderColor:self.unSelectedPlaceholderColor;
}
#pragma mark - setter
- (void)setSelectedPlaceholderFont:(UIFont *)selectedPlaceholderFont{
    _selectedPlaceholderFont = selectedPlaceholderFont;
    [self refreshPlaceholderState];
}
- (void)setUnSelectedPlaceholderFont:(UIFont *)unSelectedPlaceholderFont{
    _unSelectedPlaceholderFont = unSelectedPlaceholderFont;
    [self refreshPlaceholderState];
}
- (void)setSelectedPlaceholderColor:(UIColor *)selectedPlaceholderColor{
    _selectedPlaceholderColor = selectedPlaceholderColor;
    [self refreshPlaceholderState];
}
- (void)setUnSelectedPlaceholderColor:(UIColor *)unSelectedPlaceholderColor{
    _unSelectedPlaceholderColor = unSelectedPlaceholderColor;
    [self refreshPlaceholderState];
}

#pragma mark - lazy instantiation

- (YV_DPTF *)textField{
    if (!_textField) {
        CGRect bound = self.bounds;
        bound.origin.y = 12;
        bound.size.height -= 12;
        _textField = [[YV_DPTF alloc]initWithFrame:bound];
        _textField.dptfDelegate = self;
    }
    return _textField;
}

- (UILabel *)lblPlaceholder{
    if (!_lblPlaceholder) {
        _lblPlaceholder = [UILabel new];
        _lblPlaceholder.textColor = self.unSelectedPlaceholderColor;
        _lblPlaceholder.font = self.unSelectedPlaceholderFont;
    }
    return _lblPlaceholder;
}

- (UIColor *)unSelectedPlaceholderColor{
    if (!_unSelectedPlaceholderColor) {
        _unSelectedPlaceholderColor = [UIColor colorWithWhite:224/255.0 alpha:1];
    }return _unSelectedPlaceholderColor;
}
- (UIColor *)selectedPlaceholderColor{
    if (!_selectedPlaceholderColor) {
        _selectedPlaceholderColor = [UIColor colorWithWhite:31/255.0 alpha:1];
    }return _selectedPlaceholderColor;
}
- (UIFont *)selectedPlaceholderFont{
    if (!_selectedPlaceholderFont) {
        _selectedPlaceholderFont = [UIFont systemFontOfSize:10];
    }return _selectedPlaceholderFont;
}
- (UIFont *)unSelectedPlaceholderFont{
    if (!_unSelectedPlaceholderFont) {
        _unSelectedPlaceholderFont = [UIFont systemFontOfSize:14];
    }return _unSelectedPlaceholderFont;
}


@end
